package main;

import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/*
 * Represents an activity that elapses on different moments of time
 * This class defines all of the basic instructions that
 * must do any type of task
 */
public abstract class Task extends Node implements Observer {

	public Task(final String newName, final String newDescription) {
		super(newName, newDescription);
	}
	/*
	 * Id used for the serialize implementation
	 */
	private static final long serialVersionUID = -3766659123762571390L;
	
	public abstract double  getTime();
	public abstract void  startTimer();
	public abstract void  stopTimer();
	public abstract void  update(Observable arg0, Object arg1);
	public abstract String  print();
	public abstract Calendar  getStartTime();
	public abstract Calendar  getEndTime();
	public abstract List<Interval> getIntervals();
	public abstract boolean isActive();
	public abstract void setTopTask(Task topTask);	
	public abstract double getTimeInPeriod(Calendar startTime,
										Calendar finalTime);
	public abstract void accept(TreeVisitor visitor);
			
				
				
	
				
			
		
}