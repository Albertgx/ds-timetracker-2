package main;

/*
 * Transform a report object into 
 * a certain file type 
 */
public abstract class ReportGenerator {

		/*
	 	*Parameters: Report's object.  
	 	*Generates two sort of formats report, either Text format 
	 	*or HTML format.
		*/
		public abstract void generateReport(Report report);
		

}
