package main;


import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Represents the most basic form of a task
 */
public class BaseTask extends Task {
	
	
	/*
	 * Id for the serialize implementation
	 */
	private static final long serialVersionUID = 1493146476544948212L;
	
	private static  Logger logger = LoggerFactory.getLogger(BaseTask.class);
	
	/**
	 * This variable is used in order to define which task will be called by
	 * the notify method from clock. 
	 * This way if there is a wrapper on the base task, the update
	 * method will call the wrapper indeed the base task.
	 * 
	 * @uml.property  name="topTask"
	 */
	private Task topTask;
	
	
	/**
	 * If its true, the task is counting time
	 * @uml.property  name="active" readOnly="true"
	 */
	private boolean active = false;
	

	/** 
	 * Contains all the time intervals from the task
	 * @uml.property name="intervals"
	 * @uml.associationEnd readOnly="true" multiplicity="(0 -1)" 
	 * ordering="true" aggregation="composite" inverse="task:main.Interval"
	 */
	private List<Interval> intervals;
	
	/*
	 * Invariant method
	 */
	private boolean invariant() {
		
		boolean assertion = true;
		assertion = (assertion && (this.topTask != null));
		assertion = (assertion && (this.intervals != null));
		
		return assertion;
	}
	
	/*
	* Task constructor
	* parameters: name and description of the task
	*/
	public BaseTask(final String  name, final String description) {
		
		super(name , description);
		this.topTask = this;
		this.intervals = new ArrayList<Interval>();
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see main.Node#getTime()
	 */
	public final double getTime() {
	
		assert invariant();
		
		double time = 0;
		Interval aux;
		for (int i = 0; i < this.intervals.size(); i++) {
			aux = this.intervals.get(i);
			time = time + aux.getTranscurredTime();
		}
		assert invariant();
		
		return time;
	}

	/**
	 * Getter of the property <tt>active</tt>
	 * @return  Returns the active.
	 * @uml.property  name="active"
	 */
	public final boolean isActive() {
		return active;
	}


	/** 
	 * Getter of the property <tt>intervals</tt>
	 * @return  Returns the intervals.
	 * @uml.property  name="intervals"
	 */
	public final List<Interval> getIntervals() {
		return intervals;
	}


	/**
	 * Setter of the property <tt>topTask</tt>
	 * @param topTask  The topTask to set.
	 * @uml.property  name="topTask"
	 */
	@Override public final void setTopTask(final Task toptask) {
		this.topTask = toptask;
	}
	
	
	/*
	 * Creates a new interval of time, and turns the task active
	 */
	public final void startTimer() {
		
		assert invariant();
		
		if (this.active) {
			logger.warn("The task " + this.getName() + " has been started.");
		} else {
			logger.info("The task " + this.getName() + " has begun.");
			Clock aux = Clock.getInstance();
			aux.addObserver(topTask);
			Calendar actualDate = aux.getDate();
			Interval newInterval = new Interval(actualDate);
			this.intervals.add(newInterval);
			this.active = true;
		}
		assert invariant();
	}

		
	/*
	 * Task stops being activated and ends the current interval
	 */
	public final void stopTimer() {
		
		assert invariant();
		
		if (!this.active) {
			logger.warn("The task " + this.getName() + " hasn't been started.");
		} else {
			logger.info("The task " + this.getName() + " was stopped.");
			this.active = false;
			Clock aux = Clock.getInstance();
			aux.deleteObserver(topTask);
			
			int size = this.intervals.size();
			Interval activeInterval = this.intervals.get(size - 1);
			if (activeInterval.getTranscurredTime() == 0) {
				this.intervals.remove(activeInterval);
			}
		}
		assert invariant();
	}

	/*
	 * Refresh the elapsed time and the final time of the task
	 */
	public final void update(final Observable arg0, final Object arg1) {
		
		assert invariant();
		
		Clock clock = Clock.getInstance();
		final double timeUnit = clock.getRefreshTime();
		
		int size = this.intervals.size();
		Interval activeInterval = this.intervals.get(size - 1);
		activeInterval.addTime(timeUnit);
		activeInterval.setEndTime(clock.getDate());
		logger.debug("In " + clock.getDate().getTime().toString() 
				+ " Task" + this.getName() + ":Added " + timeUnit + " seconds");
		
		assert invariant();
	}

	
	/*
	 * (non-Javadoc)
	 * @see main.Node#print()
	 */
	public final String print() {
		
		String taskString = "";
		String nom = this.getName();
		Calendar startTime = this.getStartTime();
		Calendar endTime = this.getEndTime();
		
		long sec = (long) this.getTime();
		String time = String.format("%02d:%02d:%02d", 
				TimeUnit.SECONDS.toHours(sec), 
				TimeUnit.SECONDS.toMinutes(sec) 
				- TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(sec)),
				sec - TimeUnit.SECONDS.toMinutes(sec));
		
		String startTimeStr = "";
		String endTimeStr = "";
		if (startTime != null) {
			startTimeStr = startTime.getTime().toString();
		}
		if (endTime != null) {
			endTimeStr = endTime.getTime().toString();
		}
		
		taskString = nom + "\t" + startTimeStr + "\t" + endTimeStr 
				+ "\t" + time + "\n";
		
		return taskString;
	}


	@Override
	/*
	 * (non-Javadoc)
	 * @see main.Node#getStartTime()
	 */
	public final Calendar getStartTime() {
		
		assert invariant();
		Calendar starTime = null;
		if (this.intervals.size() > 0) {
			Interval firstInterval = this.getIntervals().get(0);
			starTime = firstInterval.getStartTime();
		}
		assert invariant();
		
		return starTime;
	}


	@Override
	/*
	 * (non-Javadoc)
	 * @see main.Node#getEndTime()
	 */
	public final Calendar getEndTime() {
		
		assert invariant();
		Calendar endTime = null;
		if (this.intervals.size() > 0) {
			int lastIntervalIndex = this.intervals.size() - 1;
			Interval lastInterval = this.getIntervals().get(lastIntervalIndex);
			endTime = lastInterval.getEndTime();
		}
		assert invariant();
		return endTime;
	}


	@Override
	public final double getTimeInPeriod(final Calendar startTime, 
										final Calendar finalTime) {
		
		assert invariant();
		double time = 0;
		for (int i = 0; i < this.intervals.size(); i++) {
			time += intervals.get(i).getTimeInPeriod(startTime, finalTime);
		}
		assert invariant();
		
		return time;
	}


	@Override
	public final void accept(final TreeVisitor visitor) {
		visitor.visit(this);
	}
}