package main;

import java.util.Calendar;
import java.util.List;
import java.util.Observable;

/*
 * Type task allows the subclasses to add functionality 
 * to a previous task encapsulated on this class
 * The functions that are not override by subclasses
 * are delegated to the encapsulated task 
 */
public abstract class TypeTask extends Task {
	
	/*
	 *Id for the serialize implementation  
	 */
	private static final long serialVersionUID = 8884098789401876014L;
	
	/*
	 * Task where will be added the new functionality
	 */
	private Task taskToAddFun;
	
	/*
	 * TypeTask constructor
	 * @param taskToAddFun task where will be added the new functionality
	 */
	public TypeTask(final Task newTaskToAddFun) {
		super(newTaskToAddFun.getName(), newTaskToAddFun.getDescription());
		this.taskToAddFun = newTaskToAddFun;
	
	}
	
	@Override
	public double getTime() {
		return this.taskToAddFun.getTime();
	}

	@Override
	public  void startTimer() {
		this.taskToAddFun.startTimer();

	}

	@Override
	public  void stopTimer() {
		this.taskToAddFun.stopTimer();

	}

	@Override
	public void update(final Observable arg0, final Object arg1) {
		this.taskToAddFun.update(arg0, arg1);

	}

	@Override
	public String print() {

		return this.taskToAddFun.print();
	}

	@Override
	public Calendar getStartTime() {
		
		return this.taskToAddFun.getStartTime();
	}

	@Override
	public Calendar getEndTime() {
		
		return this.taskToAddFun.getEndTime();
	}
	
	@Override
	public boolean isActive() {
		return this.taskToAddFun.isActive();
	}
	
	@Override 
	public void setTopTask(final Task topTask) {
		this.taskToAddFun.setTopTask(topTask);
	}
	
	public double getTimeInPeriod(Calendar startTime,Calendar finalTime){
		return this.taskToAddFun.getTimeInPeriod(startTime, finalTime);
	}
	
	public final void accept(final TreeVisitor visitor) {
		visitor.visit(this.taskToAddFun);
	}
	
	public final List<Interval>  getIntervals() {
		return this.taskToAddFun.getIntervals();
	}
	
}