package main;

import java.util.ArrayList;
import java.util.Calendar;

/*
 * Represents a brief report of the basic 
 * information about the task and projects 
 * of the tree
 */
public class BriefReport extends Report {
	
	/*
	 * Brief report constructor 
	 */
	public BriefReport(final Calendar newStartTime, 
					   final Calendar newFinalTime,
					   final Project newRoot) {
		super(newStartTime, newFinalTime, newRoot);
	}
	
	/**
	 * @uml.property  name="COLUMNS_ROOT_PROJECTS" readOnly="true"
	 */
	private static final int COLUMNS_ROOT_PROJECTS = 4;
	
	/*
	 * Generates a report, filling the report content
	 * with all the information 
	 */
	public final void genReport() {
		assert this.invariant();
		this.getContent().addHoritzontalLine();
		this.getContent().addTitle("Brief report");
		this.getContent().addHoritzontalLine();
		this.getContent().addSubTitle("Period");
		
		//Period table
		Taula table = new Taula(0, 2);
		ArrayList<String> stringList = new ArrayList<String>();
		stringList.add("");
		stringList.add("Date");
		table.afegeixFila(stringList);
		
		stringList = new ArrayList<String>();
		stringList.add("Since");
		stringList.add(this.getStartTime().getTime().toString());
		table.afegeixFila(stringList);
		
		stringList = new ArrayList<String>();
		stringList.add("To");
		stringList.add(this.getFinalTime().getTime().toString());
		table.afegeixFila(stringList);
		
		stringList = new ArrayList<String>();
		stringList.add("Report generation date");
		stringList.add(this.getGenDate().getTime().toString());
		table.afegeixFila(stringList);
		
		this.getContent().addTable(table, true);
		this.getContent().addHoritzontalLine();
		this.getContent().addSubTitle("Root projects");
		
		//Root projects table
		table = new Taula(0, COLUMNS_ROOT_PROJECTS);
		stringList = new ArrayList<String>();
		stringList.add("Project");
		stringList.add("Start time");
		stringList.add("Final time");
		stringList.add("Elapsed time");
		table.afegeixFila(stringList);
		
		Project root = this.getRoot();
		
		int numProjects = root.getSubprojects().size();
		
		
		// If time > 0 means project is inside period Time and 
		//therefore we must add it.
		double time = 0;
		for (int i = 0; i < numProjects; i++) {
			
			Node subProject = root.getSubProject(i);
			time = subProject.getTimeInPeriod(this.getStartTime(),
					this.getFinalTime());
			if (time > 0) {
				stringList = new ArrayList<String>();
				stringList.add(subProject.getName());
				stringList.add(subProject.getStartTime().getTime().toString());
				stringList.add(subProject.getEndTime().getTime().toString());
				stringList.add(Double.toString(time));
				table.afegeixFila(stringList);
			}
		}
		this.getContent().addTable(table, false);
		this.getContent().addHoritzontalLine();
		assert this.invariant();
	}	
}