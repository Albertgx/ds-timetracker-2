package main;

import java.util.Calendar;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Brings the current time information 
 * and sends an update to all active tasks 
 * in order to count the elapsed time.
 */
public final class Clock extends Observable {
		
		private static Logger logger = LoggerFactory.getLogger(Clock.class);
		
		/**
		 * @uml.property  name="instance"
		 */
		private static Clock instance = null;
		
		/**
		 * @uml.property  name="refreshTime" readOnly="true"
		 */
		private static long refreshTime = 1;
		
		
		/*
		 * Is responsible of calling the tick() function each period of time
		 * in order to call active tasks on a time update
		 *
		 */
		public class ClockTimer extends Thread {
			/*
			 * Calls the tick method until the
			 * thread stops
			 */
			public final void run() {
				Clock clock = Clock.getInstance();
				while (true) {
					try {
						clock.tick();
					} catch (InterruptedException e) {
						logger.error("Error creating the thread of the class " 
										+ "ClockTimer");
					}
				}
			}
		}
		
		private boolean invariant() {
			
			boolean assertion = true;
			assertion = Clock.refreshTime > 0;
			return assertion;
		}

		/*
		 * Clock constructor 
		 * It creates a instance of Clock Timer in order to
		 * call to all active tasks every elapsed refresh period of time
		 */
		private Clock() {
			logger.debug("Timer started");
			ClockTimer timer = new ClockTimer();
			timer.start(); 	
		}
		
		
		/*
		 * Return an existing instance of clock if it already exists
		 * or creates a new one if there isn't any. 
		 */
		public static Clock getInstance() {
			if (instance == null) {
				instance = new Clock();
			}
			return instance;
		}
		
		
		/*
		 * Return the time info (Calendar instance) from the time moment
		 * this function is called
		 */
		public Calendar getDate() {
			assert invariant();
			Calendar actualDay = Calendar.getInstance();
			assert invariant();
			return actualDay;
		}

			
		/**
		 * Calls every active task in order to count it's elapsed time
		 * @throws InterruptedException 
		 */
		public void tick() throws InterruptedException {
			assert invariant();
			TimeUnit.SECONDS.sleep(refreshTime);
			setChanged();
			notifyObservers();
			assert invariant();
		}

			
		/*
		 * Getter for the property refreshTime
		 * Return the time period to wait to call the tick method 
		 */
		public long getRefreshTime() {
			return Clock.refreshTime;
		}

			
		/*
		 * Setter for the property refreshTime
		 * parameter: time period to wait to call the tick method 
		 */
		public void setRefreshTime(final long newrefreshTime) {
			Clock.refreshTime = newrefreshTime;
		}
}
