package main;

import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Wrapper for the baseTask class that allows to set to
 * a task a time limit
 */
public class TimeLimit extends TypeTask implements Observer {
	
	/*
	 * Id for the serialize implementation 
	 */
	private static final long serialVersionUID = -7840597084173084066L;
	
	private double timeLimit;
	
	private static Logger logger = LoggerFactory.getLogger(TimeLimit.class);
	
	/*
	 * Is true if the task has reached the time limit
	 */
	private boolean ended;
	
	/*
	 * Time limit constructor
	 * parameters: newTimeLimit: max time for the task
	 */
	public TimeLimit(final Task taskToAddFun, final double newTimeLimit) {
		super(taskToAddFun);
		super.setTopTask(this);

		this.timeLimit = newTimeLimit;
		this.ended = false;
		
	}
	
	/*
	 * Starts the timer only if the task has not reached
	 * the time limit. 
	 */
	public final void startTimer() {
		if (!ended) {
			super.startTimer();
		
		} else {
			logger.warn("Aquesta tasca ja ha acabat, " 
		+ "s'ha superat el temps limit");
		}
	}
	
	/*
	 * For each update counts a unit of time and check if
	 * the task has reached the time limit
	 */
	public final void update(final Observable arg0, final Object arg1) {
		
		
		super.update(arg0, arg1);
		double actualTime = super.getTime();
		if (actualTime >= this.timeLimit) {
			logger.info("S'ha superat el temps limit de la tasca " 
						+ super.getName());
			super.stopTimer();
			this.ended = true;
		}
	}
}