package main;

import java.util.ArrayList;
import java.util.Calendar;

/*
 * Generates report content in function of
 * which element of the tree visits
 */
public class ReportSubProjects extends TreeVisitor {
	
	public  static final  int NUM_COLUMNS_SUBPROJECT_TALBE = 5;
	public  static final  int NUM_COLUMNS_TASK_TALBE = 5;
	public  static final  int NUM_COLUMNS_INTERVAL_TABLE = 6;
	
	/**
	 * Contains all the sub projects inside the report period
	 * @uml.property  name="subProjects"
	 */
	private Taula subProjects;
	/**
	 * Start time of the report period
	 * @uml.property  name="startTime"
	 */
	private Calendar startTime;
	/**
	 * Final time of the report period
	 * @uml.property  name="finalTime"
	 */
	private Calendar finalTime;
	/**
	 * Hierarchy of the node visiting
	 * @uml.property  name="hierarchy"
	 */
	private String hierarchy;
	/**
	 * All tasks inside the report period 
	 * @uml.property  name="tasks" readOnly="true"
	 */
	private Taula tasks;
	/**
	 * Number of the interval visiting
	 * @uml.property  name="numInterval"
	 */
	private int numInterval;
	/** All the intervals inside the report period
	 * @uml.property  name="intervals"
	 */
	private Taula intervals;
	/**
	 * Name of the task visiting, or the father task
	 * @uml.property  name="taskName"
	 */
	private String taskName;
	
	private boolean invariant() {
		boolean assertion = true;
		assertion = (assertion && this.subProjects != null);
		assertion = (assertion && this.tasks != null);
		assertion = (assertion && this.intervals != null);
		assertion = (assertion && this.numInterval >= 0);
		assertion = (assertion && this.taskName != null);
		
		return assertion; 
	}
	
	/*
	 * Class constructor
	 * parameters: start and end time of the report period and the hierarchy 
	 * of the node to visit
	 */
	public ReportSubProjects(final String newHierarchy, 
			final Calendar newStartTime, final Calendar newFinalTime) {
		this.hierarchy = newHierarchy;
		this.startTime = newStartTime;
		this.finalTime = newFinalTime;
		this.subProjects = new Taula(0, NUM_COLUMNS_SUBPROJECT_TALBE);
		this.tasks = new Taula(0, NUM_COLUMNS_TASK_TALBE);
		this.intervals = new Taula(0, NUM_COLUMNS_INTERVAL_TABLE);
		this.numInterval = 0;
		this.taskName = "";
	}
	
	@Override
	/*
	 * If the visited project is a sub project and is inside
	 * the period ,is added to subProjects
	 * If is inside the period it also visits all the sub projects
	 * of project
	 */
	public final void visit(final Project project) {
		this.invariant();
		
		int numProjects = project.getSubprojects().size();
		double time = project.getTimeInPeriod(this.startTime, this.finalTime);
		if (time > 0) {
			if (this.hierarchy.indexOf(".") > 0) {
				ArrayList<String> stringList =  new ArrayList<String>();
				stringList.add(this.hierarchy);
				stringList.add(project.getName());
				stringList.add(project.getStartTime().getTime().toString());
				stringList.add(project.getEndTime().getTime().toString());
				stringList.add(Double.toString(time));
				this.subProjects.afegeixFila(stringList);	
			}
			for (int i = 0; i < numProjects; i++) {
				
				String numSubProject = "";
				if (this.hierarchy != "") {
					numSubProject += ".";
				}
				numSubProject += Integer.toString(i + 1);
				ReportSubProjects reportVisitor = new ReportSubProjects(
						this.hierarchy + numSubProject, 
						this.startTime, this.finalTime);
				project.getSubProject(i).accept(reportVisitor);
				this.subProjects.afegirTaula(reportVisitor.getSubProjects());
				this.tasks.afegirTaula(reportVisitor.getTasks());
				this.intervals.afegirTaula(reportVisitor.getIntervals());
			}	
		}
		
		this.invariant();
		
	}

	@Override
	/*
	 * If the task is inside the period, is added to tasks and 
	 * visits all the intervals that contains
	 */
	public final void visit(final Task task) {
		
		this.invariant();
		double time = task.getTimeInPeriod(this.startTime, this.finalTime);
		if (time > 0) {
			ArrayList<String> stringList = new ArrayList<String>();
			String newHierarchy = this.hierarchy;
			int lastPointIndex = newHierarchy.lastIndexOf(".");
			newHierarchy = newHierarchy.substring(0, lastPointIndex);
			stringList.add(newHierarchy);
			stringList.add(task.getName());
			stringList.add(task.getStartTime().getTime().toString());
			stringList.add(task.getEndTime().getTime().toString());
			stringList.add(Double.toString(time));
			this.tasks.afegeixFila(stringList);
			
			int numIntervals = task.getIntervals().size();
			for (int i = 0; i < numIntervals; i++) {
				ReportSubProjects reportVisitor = new ReportSubProjects(
						newHierarchy, this.startTime, this.finalTime);
				reportVisitor.setNumInterval(i + 1);
				reportVisitor.setTaskName(task.getName());
				task.getIntervals().get(i).accept(reportVisitor);
				this.intervals.afegirTaula(reportVisitor.intervals);
				
			}
			
		}
		this.invariant();
	}

	@Override
	/*
	 * If the interval is inside the period then 
	 * the visited interval is added to intervals
	 */
	public final void visit(final Interval interval) {
		
		this.invariant();
		double time = interval.getTimeInPeriod(this.startTime, this.finalTime);
		if (time > 0) {
			ArrayList<String> stringList = new ArrayList<String>();
			stringList.add(this.hierarchy);
			stringList.add(this.taskName);
			stringList.add(String.valueOf(this.numInterval));
			stringList.add(interval.getStartTime().getTime().toString());
			stringList.add(interval.getEndTime().getTime().toString());
			stringList.add(String.valueOf(time));
			this.intervals.afegeixFila(stringList);
		}
		this.invariant();

	}



	/**
	 * Getter of the property <tt>subProjects</tt>
	 * @return  Returns the subProjects.
	 * @uml.property  name="subProjects"
	 */
	public final Taula getSubProjects() {
		return subProjects;
	}
	/**
	 * Getter of the property <tt>startTime</tt>
	 * @return  Returns the startTime.
	 * @uml.property  name="startTime"
	 */
	public final Calendar getStartTime() {
		return startTime;
	}

	/**
	 * Getter of the property <tt>finalTime</tt>
	 * @return  Returns the finalTime.
	 * @uml.property  name="finalTime"
	 */
	public final Calendar getFinalTime() {
		return finalTime;
	}

	/**
	 * Getter of the property <tt>hierarchy</tt>
	 * @return  Returns the hierarchy.
	 * @uml.property  name="hierarchy"
	 */
	public final String getHierarchy() {
		return hierarchy;
	}
	/**
	 * Getter of the property <tt>tasks</tt>
	 * @return  Returns the tasks.
	 * @uml.property  name="tasks"
	 */
	public final Taula getTasks() {
		return tasks;
	}
	/**
	 * Getter of the property <tt>numInterval</tt>
	 * @return  Returns the numInterval.
	 * @uml.property  name="numInterval"
	 */
	public final int getNumInterval() {
		return numInterval;
	}

	/**
	 * Setter of the property <tt>numInterval</tt>
	 * @param numInterval  The numInterval to set.
	 * @uml.property  name="numInterval"
	 */
	public final void setNumInterval(final int newNumInterval) {
		this.numInterval = newNumInterval;
	}



	/**
	 * Getter of the property <tt>intervals</tt>
	 * @return  Returns the intervals.
	 * @uml.property  name="intervals"
	 */
	public final Taula getIntervals() {
		return intervals;
	}
	
	/**
	 * Setter of the property <tt>taskName</tt>
	 * @param taskName  The taskName to set.
	 * @uml.property  name="taskName"
	 */
	public final void setTaskName(final String newTaskName) {
		this.taskName = newTaskName;
	}





		
}
