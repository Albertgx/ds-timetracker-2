package main;

import java.util.Calendar;

/*
 * Represents a report about the time data
 * about tasks and projects within a defined
 * period 
 */
public abstract class Report {
	
	/**
	 * @uml.property  name="startTime" readOnly="true"
	 */
	private Calendar startTime;
	/**
	 * @uml.property  name="finalTime"
	 */
	private Calendar finalTime;
	/**
	 * @uml.property  name="genDate"
	 */
	private Calendar genDate;
	
	/**
	 * @uml.property  name="content"
	 * @uml.associationEnd  readOnly="true" 
	 * aggregation="composite" inverse="report:main.Content"
	 */
	private Content content;


 
	protected final boolean invariant() {
		boolean assertion = true;
		assertion = (assertion && this.root != null);
		assertion = (assertion && this.content != null);
		assertion = (assertion && this.genDate != null);
		assertion = (assertion && this.startTime != null);
		assertion = (assertion && this.finalTime != null);
		assertion = (assertion && this.startTime.before(this.finalTime));
		return assertion;
	}	
	
	/*
	 * Report constructor 
	 */
	public Report(final Calendar newStartTime
			, final Calendar newFinalTime, final Project newRoot) {
		this.startTime = newStartTime;
		this.finalTime = newFinalTime;
		this.genDate = Calendar.getInstance();
		this.content = new Content();
		this.root = newRoot;
	}

	public abstract void genReport();
	
	/** 
	 * @uml.property name="root"
	 * @uml.associationEnd readOnly="true" inverse="report:main.Project"
	 */
	private Project root;

	/** 
	 * Getter of the property <tt>root</tt>
	 * @return  Returns the root.
	 * @uml.property  name="root"
	 */
	public final Project getRoot() {
		return root;
	}
	
	/**
	 * Getter of the property <tt>content</tt>
	 * @return  Returns the content.
	 * @uml.property  name="content"
	 */
	
	public final Content getContent() {
		return this.content;
	}
	
	
		
	
	public final Calendar getStartTime() {
		return this.startTime;
	}
	
	public final Calendar getFinalTime() {
		return this.finalTime;
	}
	
	public final Calendar getGenDate() {
		return this.genDate;
	}
}
