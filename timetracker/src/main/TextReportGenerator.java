package main;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * This class transforms a report object into
 * a text file 
 */
public class TextReportGenerator extends ReportGenerator {
	
	
	private static Logger logger = 
			LoggerFactory.getLogger(TextReportGenerator.class);
	
	/*
	*It generates a text format output file. 
	*/
	public final void generateReport(final Report report) {
		Content content = report.getContent();
		String text = "";
		String element;
		
		for (int i = 0; i < content.getNumElements(); i++) {
			
			element = content.getElement(i);
			int index = 0;
			Taula table;
			
			switch (element) { 
			
			case "line": 	text += "----------------------------------" 
							+ "----------------------------------------" + "\n";
							break;
			
			case "title": 	text += content.getValue(i) + "\n";
							break;
			
			case "subtitle":text += content.getValue(i) + "\n";
							break;
			
			case "description": text += content.getValue(i) + "\n";
								break;	
							
			case "table": 	index = Integer.parseInt(content.getValue(i));
							table = content.getTable(index);
							for (int r = 1; r <= table.getNfiles(); r++) {
								for (int c = 1; c <= table.getNcolumnes(); 
										c++) {
									text += table.getPosicio(r, c);
									text += "\t";
								}
								text += "\n";
							}
							break;
			
			case "tableheader": index = Integer.parseInt(content.getValue(i));
								table = content.getTable(index);
								for (int r = 1; r <= table.getNfiles(); r++) {
									for (int c = 1; c <= table.getNcolumnes(); 
											c++) {
										text += table.getPosicio(r, c);
										text += "\t";
									}
									text += "\n";
								}
								break;
							
			default: logger.error("Errors in content elements");
			
			}
			
		}
		
		
		String filename = "Reports/Report ";
		filename += report.getGenDate().getTime().toString();
		filename += ".txt";
		filename = filename.replaceAll(":", "_");
		try {
			System.out.println(filename);
			PrintWriter  out = new PrintWriter(filename);
			out.println(text);
			out.close();
		
		} catch (FileNotFoundException e) {
			logger.error("Error creating a file");
		}
		
		
	
		
		
		
	}

}
