package main;

import java.io.Serializable;
import java.util.Calendar;


/*
 * It represents a time event that elapses on a different moments of time
 */
public abstract class Node implements Serializable {
	
	
	/*
	 * Id for the serialize implementation
	 */
	private static final long serialVersionUID = 5807456182294216543L;
	
	
	/**
	 * Name of the node
	 * @uml.property  name="name" readOnly="true"
	 */
	private String name;


	/**
	 * Aditional information about the node
	 * @uml.property  name="description" readOnly="true"
	 */
	private String description;
	
	
	/*
	 * Invariant method
	 */
	private boolean invariant() {
		
		boolean assertion = true;
		assertion = ((this.name != null) && (this.description != null));
		
		return assertion;
	}
	
	
	/*
	 * Node constructor
	 */
	public Node(final String newName, final String newDescription) {
		this.name = newName;
		this.description = newDescription;
	}


	/**
	 * Getter of the property <tt>name</tt>
	 * @return  Returns the name.
	 * @uml.property  name="name"
	 */
	public final String getName() {
		
		assert invariant();
		return this.name;
	}


	/**
	 * Getter of the property <tt>description</tt>
	 * @return  Returns the description.
	 * @uml.property  name="description"
	 */
	public final String getDescription() {
		
		assert invariant();
		return this.description;
	}

		
	/*
	 * Return the elapsed time that this node have been active
	 */
	public abstract double getTime();


	
	
	/*
	 * Prints the name of the node, start time, end time and elapsed time
	 */
	public abstract String print();
		


			
	/*
	 * Return at first time the node starts
	 */
	public abstract Calendar getStartTime();


	
	/*
	 * Return the last time the node ends
	 */
	public abstract Calendar getEndTime();


		
	/*
	 * Return the elapsed time in a defined period 
	 * parameters: start and final time that define period
	 */
	public abstract double getTimeInPeriod(Calendar startTime, 
										Calendar finalTime);


	/*
	 * 
	 */
	public abstract void accept(TreeVisitor visitor);
}