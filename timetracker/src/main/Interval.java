package main;

import java.io.Serializable;
import java.util.Calendar;

/*
 * This class is used for counting the elapsed time from a 
 * moment we start the task until we stop it 
 */
public class Interval implements Serializable {
	
	static final int MILSEC_TO_SEC = 1000;
	
	/*
	 * Id for the serialize implementation 
	 */
	private static final long serialVersionUID = 8813296402050486686L;
	
	/**
	 * @uml.property  name="startTime" readOnly="true"
	 */
	private final Calendar startTime;

	/**
	 * @uml.property  name="endTime" readOnly="true"
	 */
	private Calendar endTime;
	

	/**
	 * @uml.property  name="transcurredTime" readOnly="true"
	 */
	private double transcurredTime = 0;
	
	
	/*
	 * Invariant method
	 */
	private boolean invariant() {
		
		boolean assertion = true;
		assertion = (this.transcurredTime >= 0) && (this.startTime != null);
	
		return assertion;
	}
	
	/*
	 * Interval constructor
	 * parameter: time that interval starts counting time
	 */
	public Interval(final Calendar newStartTime) {
		
		this.startTime = newStartTime;
	}
	
	
	/**
	 * Getter of the property <tt>startTime</tt>
	 * @return  Returns the startTime.
	 * @uml.property  name="startTime"
	 */
	public final Calendar getStartTime() {
		
		return startTime;
	}

	/**
	 * Getter of the property <tt>endTime</tt>
	 * @return  Returns the endTime.
	 * @uml.property  name="endTime"
	 */
	public final Calendar getEndTime() {
		
		return endTime;
	}


	/*
	 * Setter of the property endTime
	 * parameter: moment of time the interval has ended 
	 * 				or the actual time if is active 
	 */
	public final void setEndTime(final Calendar newEndTime) {
		
		this.endTime = newEndTime;
	}
	
	/**
	 * Getter of the property <tt>transcurredTime</tt>
	 * @return  Returns the transcurredTime.
	 * @uml.property  name="transcurredTime"
	 */
	public final double getTranscurredTime() {
		
		return transcurredTime;
	}

					
			
	/*
	 * Add time to the elapsed time of the interval
	 * parameter: amount of time to add to the elapsed time
	 */
	public final void addTime(final double time) {
		
		assert invariant();
		this.transcurredTime = this.transcurredTime + time;
		assert invariant();
	}


		
	/*
	 * Returns the elapsed time in a defined period 
	 * parameters: start and final time of define period
	 */
	public final double getTimeInPeriod(final Calendar startTimePer, 
										final Calendar finalTimePer) {
		
		assert invariant();
		
		double time = this.transcurredTime;
		if (this.startTime.before(startTimePer) 
				&& this.endTime.before(startTimePer)) {
			return 0;
		}
		
		if (this.startTime.after(finalTimePer) 
				&& this.endTime.after(finalTimePer)) {
			return 0;
		}
		
		//Intersection with period start
		if (this.startTime.before(startTimePer)
				&& this.endTime.after(startTimePer)) {
				
			double overTime = (startTimePer.getTimeInMillis()
					- this.startTime.getTimeInMillis()) / MILSEC_TO_SEC;
			
			time = time - Math.abs(overTime);
		}
		
		//Intersection with period end
		if (this.endTime.after(finalTimePer) 
				&& this.startTime.before(finalTimePer)) {
			
			double overTime = (finalTimePer.getTimeInMillis() 
					- this.endTime.getTimeInMillis()) / MILSEC_TO_SEC;
			
			time = time - Math.abs(overTime);
		}
		assert invariant();
		
		return time;
	}
	
	public final void accept(final TreeVisitor visitor) {
		visitor.visit(this);
	}
	
	
}