package main;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * This class transforms a report object into
 * a HTML file 
 */
public class HtmlReportGenerator extends ReportGenerator {

	static final int SIZE_TITLE = 2;
	static final int SIZE_SUBTITLE = 4;
	
	private static Logger logger = 
			LoggerFactory.getLogger(HtmlReportGenerator.class);
	
	/*
 	*It generates a HTML format output file. 
 	*/
	public final void generateReport(final Report report) {
		Content content = report.getContent();
		PaginaWeb html = new PaginaWeb();
		String element;
		
		for (int i = 0; i < content.getNumElements(); i++) {
			element = content.getElement(i);
			int index = 0;
			Taula table;
			switch (element) {
				case "line":
					html.afegeixLiniaSeparacio(); 
					break;
				case "title":
					html.afegeixHeader(content.getValue(i), SIZE_TITLE , true);
					break;
				case "subtitle":
					html.afegeixHeader(content.getValue(i), SIZE_SUBTITLE 
							, false);
					break;
				case "description":
					html.afegeixTextNormal(content.getValue(i));
					break;
				case "table":
					index = Integer.parseInt(content.getValue(i));
					table = content.getTable(index); 
					html.afegeixTaula(table.getTaula(), true , false);
					break;
				case "tableheader":
					index = Integer.parseInt(content.getValue(i));
					table = content.getTable(index); 
					html.afegeixTaula(table.getTaula(), true , true);
					break;
				default:
					logger.error("Errors in content elements");
			}
		}
	
		//How to save an HTML file
		String filename = "Reports/Report ";
		filename += report.getGenDate().getTime().toString();
		filename += ".html";
		filename = filename.replaceAll(":", "_");
		try {
			System.out.println(filename);
			PrintWriter  out = new PrintWriter(filename);
			out.println(html.getPagina());
			out.close();
		
		} catch (FileNotFoundException e) {
			logger.error("Error creating a file");
		}	
	}
}