package main;

import java.util.ArrayList;
import java.util.List;

/*
 * This class stores report contents in order
 * to be transformed into files later 
 */
public class Content {
		

		/**
		 * Types of the elements contained on the object 
		 * @uml.property  name="elements"
		 */
		private List<String> elements;
		
		/**
		 * Complementary list of elements that 
		 * contains the value of each element
		 * @uml.property  name="values" readOnly="true"
		 */
		private List<String> values;
		
		/**
		 * Contains all the tables added
		 * @uml.property  name="tables" readOnly="true"
		 */
		private List<Taula> tables;


		/**
		 * Getter of the property <tt>tables</tt>
		 * @return  Returns the tables.
		 * @uml.property  name="tables"
		 */
		public final List<Taula> getTables() {
			return tables;
		}

		/**
		 * Getter of the property <tt>values</tt>
		 * @return  Returns the values.
		 * @uml.property  name="values"
		 */
		public final List<String> getValues() {
			return values;
		}
		
		private boolean invariant() {
			
			boolean assertion = true;
			assertion = (assertion && this.elements != null);
			assertion = (assertion && this.values != null);
			assertion = (assertion && this.tables != null);
			return assertion; 
		}
		
		/*
		 * Content constructor 
		 */
		public Content() {
			
			this.elements = new ArrayList<String>();
			this.values = new ArrayList<String>();
			this.tables = new ArrayList<Taula>();
		}
		
		/*
		 * Adds an horitzontal line to the content
		 */
		public final void addHoritzontalLine() {
			
			assert this.invariant();
			this.elements.add("line");
			this.values.add("");
			assert this.invariant();
		}

			
		/*
		 * Adds the specified title to the content
		 */
		public final void addTitle(final String title) {
			
			assert this.invariant();
			this.elements.add("title");
			this.values.add(title);
			assert this.invariant();
		}


		
		/*
		 *  Adds the specified subtitle to the content
		 */
		public final void addSubTitle(final String subTitle) {
			
			assert this.invariant();
			this.elements.add("subtitle");
			this.values.add(subTitle);
			assert this.invariant();
		}
			

		/*
		 *  Adds the specified description to the content
		 */
		public final void addDescription(final String description) {
			
			assert this.invariant();
			this.elements.add("description");
			this.values.add(description);
			assert this.invariant();
		}


		/*
		 *  Adds a table to the content
		 *  parameters: table to add and if the first column is a header or not
		 */
		public final void addTable(final Taula table, 
								   final boolean firstColHeader) {
			
			assert this.invariant();
			this.tables.add(table);
			if (firstColHeader) {
				this.elements.add("tableheader");
			} else {
				this.elements.add("table");
			}
			this.values.add(Integer.toString(this.tables.size() - 1));
			assert this.invariant();
		}


		public final String getElement(final int i) {
			return this.elements.get(i);	
		}
		
		public final String getValue(final int i) {
			return this.values.get(i);
		}
		
		public final Taula getTable(final int i) {
			return this.tables.get(i);
		}

		public final int getNumElements() {
			return this.elements.size();
		}
}