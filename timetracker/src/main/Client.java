package main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.util.Calendar;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Client {
	
	private Client() {
		//Not called
	}
	
	private static Logger logger = LoggerFactory.getLogger(Client.class);
	
	/*
	 * Definitions for the switch case and removal magic numbers
	 */
	static final int ZERO  = 0;
	static final int ONE   = 1;
	static final int TWO   = 2;
	static final int THREE = 3;
	static final int FOUR  = 4;
	static final int FIVE  = 5;
	static final int SIX   = 6;
	static final int SEVEN = 7;
	static final int EIGHT = 8;
	static final int NINE  = 9;
	static final int TEN   = 10;
	static final int REPORT_TEST_PERIOD_START = 4;
	static final int REPORT_TEST_PERIOD_END = 14;
	static final int REPORT_TEST_WAIT_1 = 4;
	static final int REPORT_TEST_WAIT_2 = 6;
	static final int REPORT_TEST_WAIT_3 = 4;
	static final int REPORT_TEST_WAIT_4 = 2;
	static final int REPORT_TEST_WAIT_5 = 4;
	
	/*
	 * @param args
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(final String[] args) throws InterruptedException {
		
		/*testA1();
		testA2();
		menu();*/
		testReport();
	}
	
	/*
	 * Saves all the tree project in a binary file
	 */
	public static void saveTree(final Project root) {
		
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream("Tree.bin"));
			oos.writeObject(root);
		} catch (IOException e) {
			logger.error("Error to creating file");
		}
	}
	
	/*
	 * Loads an existing tree in a binary file
	 */
	public static Project loadTree() {
		
		Project root = null;
		ObjectInputStream ois;
		
		try {
			ois = new ObjectInputStream(new FileInputStream("Tree.bin"));
			root = (Project) ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
		
			logger.error("Error to reading file");
		}
		return root;
	}
	
	
	public static void menu() {
		
		Scanner keyInput = new Scanner(System.in);
		boolean exit = false;
		int input = 1;
	
		String name = "";
		Task auxTask;
		Calendar scheduledTime = null;
		
		
		Project root = new Project("root", "");
		Project actProject;
		TaskManager taskManager = new TaskManager(root);
		root = taskManager.getRoot();
		actProject = root;

		System.out.println("--- TIME TRACKER ---");
		
		while (!exit) {
			System.out.println("1.Show tasks/projects\n" 
								+ "2.Select project\n" 
								+ "3.Add project\n"  
								+ "4.Add task\n" 
								+ "5.Add task(time limit)\n" 
								+ "6.Add task(scheduled time)\n" 
								+ "7.Add task(all options)\n" 
								+ "8.Start task\n" 
								+ "9.Stop task\n" 
								+ "0.Exit\n");

			input = keyInput.nextInt();
			
		
			switch (input) {
			
				case ONE: taskManager.printTable();
				     	break;
				
				case TWO: System.out.println("Select a project (1-N)");
						input = keyInput.nextInt();
						if (input == 0) {
							actProject = root;
						} else {
							try {
								actProject = (Project) 
										actProject.getSubProject(input - 1);
								} catch (Exception e) {
									logger.error("You have not selected "
															+ "a project ");
								}	
						}
				  		break;
				
				case THREE: System.out.println("Enter the name "
													+ "of project: ");
						name = keyInput.next();
						actProject.newProject(name, "");
						break;
				
				case FOUR: System.out.println("Enter the name "
													+ "of task: ");
						name = keyInput.next();
						actProject.newTask(name, "");
						break;
						
				case FIVE: System.out.println("Enter the name "
													+ "of task:");
						name = keyInput.next();
						System.out.println("Enter the time limit");
						input = keyInput.nextInt();
						actProject.newTaskTimeLimit(name, "", input);
						break;
						
				case SIX: System.out.println("Enter the name of task: ");
						name = keyInput.next();
						System.out.println("Enter how many seconds you want "
										  		+ "the task start \n");
						input = keyInput.nextInt();
						
						scheduledTime = Calendar.getInstance();
						scheduledTime.add(Calendar.SECOND, input);
						
						actProject.newTaskScheduledTime(name, "", 
																scheduledTime);
						break;
						
				case SEVEN: System.out.println("Enter the name "
															+ "of task: ");
						name = keyInput.next();
						System.out.println("Enter how many seconds you want "
											+ "the task start ");
						input = keyInput.nextInt();
						System.out.println("Enter the time limit");
						int tempsLimit = keyInput.nextInt();
						
					
						scheduledTime = Calendar.getInstance();
						scheduledTime.add(Calendar.SECOND, input);
						
						actProject.newTaskScheduledTimeLimit(name, "", 
											(double) tempsLimit, scheduledTime);
						break;
			
						
				case EIGHT: System.out.println("Select a subtask (1-N)");
				  		input = keyInput.nextInt();
				  		try {
				  			auxTask = 
				  					(Task) actProject.getSubProject(input - 1);
				  			auxTask.startTimer();
				  			} catch (Exception e) {
				  				logger.error("You have not selected a task");
				  		}
					    break; 
					    
				case NINE: System.out.println("Select a subtask (1-N)");
				  		input = keyInput.nextInt();
						try {
							auxTask = 
									(Task) actProject.getSubProject(input - 1);
							auxTask.stopTimer();
							} catch (Exception e) {
								logger.error("You have not selected a task");
						}
					    break; 
				 
				case ZERO: exit = true;
						break;
						
				default:
						logger.error("Invalid option. Enter a  "
											+ "valid option.");
						break;
			}
			
		}
	}
	
	public static void testA1() throws InterruptedException {
		
		logger.info("The testA1 has been started");
		Project root = new Project("root", "");
		TaskManager taskManager = new TaskManager(root);

		Project explorer;
		explorer = taskManager.getRoot();
		explorer.newProject("P1", "");
		explorer = (Project) explorer.getSubProject(0);
		explorer.newTask("T3", "");
		explorer.newProject("P2", "");
		explorer = (Project) explorer.getSubProject(1);
		explorer.newTask("T1", "");
		explorer.newTask("T2", "");
		
		
		explorer = taskManager.getRoot();
		explorer = (Project) explorer.getSubProject(0);
		Task task3 = (Task) explorer.getSubProject(0);
		task3.startTimer();
		
		TimeUnit.SECONDS.sleep(THREE);
		
		task3.stopTimer();

		taskManager.printTable();
		
		
		TimeUnit.SECONDS.sleep(SEVEN);
		
		explorer = (Project) explorer.getSubProject(1);
		Task task2 = (Task) explorer.getSubProject(1);
		task2.startTimer();
		
		TimeUnit.SECONDS.sleep(TEN);
		
		task2.stopTimer();
		task3.startTimer();
		
		TimeUnit.SECONDS.sleep(TWO);
		
		task3.stopTimer();
		
		System.out.println("--- TEST A1 ---\n");
		taskManager.printTable();
		
		saveTree(taskManager.getRoot());
		
	}
	
	
	public static void testA2() throws InterruptedException {
		
		logger.info("The testA2 has been started");
		Project root = new Project("root", "");
		TaskManager taskManager = new TaskManager(root);
		Clock clock = Clock.getInstance();
		clock.setRefreshTime(2);
		
		Project explorer;
		explorer = taskManager.getRoot();
		explorer.newProject("P1", "");
		explorer = (Project) explorer.getSubProject(0);
		explorer.newTask("T3", "");
		explorer.newProject("P2", "");
		explorer = (Project) explorer.getSubProject(1);
		explorer.newTask("T1", "");
		explorer.newTask("T2", "");
		
		explorer = taskManager.getRoot();
		explorer = (Project) explorer.getSubProject(0);
		Task task3 = (Task) explorer.getSubProject(0);
		task3.startTimer();
		explorer = (Project) explorer.getSubProject(1);
		Task task2 = (Task) explorer.getSubProject(1);
		Task task1 = (Task) explorer.getSubProject(0);
		
		TimeUnit.SECONDS.sleep(FOUR);
		
		task2.startTimer();
		
		TimeUnit.SECONDS.sleep(TWO);
		
		task3.stopTimer();
		
		TimeUnit.SECONDS.sleep(TWO);
		
		task1.startTimer();
		
		TimeUnit.SECONDS.sleep(FOUR);
		
		task1.stopTimer();
		
		TimeUnit.SECONDS.sleep(TWO);
		
		task2.stopTimer();
		
		TimeUnit.SECONDS.sleep(FOUR);
		
		task3.startTimer();
		
		TimeUnit.SECONDS.sleep(TWO);
		
		task3.stopTimer();
		
		System.out.println("--- TEST A2 ---\n");
		taskManager.printTable();
		saveTree(taskManager.getRoot());
	}
	
	public static void testReport() throws InterruptedException {
		
		//Test tree creation
		Project root = new Project("root", "");
		TaskManager taskManager = new TaskManager(root);
		
		root.newProject("P1", "");
		root.newProject("P2", "");
		Project explorer = (Project) root.getSubProject(1);
		explorer.newTask("T3", "");
		Task t3 = (Task) explorer.getSubProject(0);
		
		explorer = root;
		explorer = (Project) explorer.getSubProject(0);
		explorer.newProject("P1.2", "");
		explorer.newTask("T1", "");
		explorer.newTask("T2", "");
		Task t1 = (Task) explorer.getSubProject(1);
		Task t2 = (Task) explorer.getSubProject(2);
		
		explorer = (Project) explorer.getSubProject(0);
		explorer.newTask("T4", "");
		Task t4 = (Task) explorer.getSubProject(0);
		
		Calendar startTime = Calendar.getInstance();
		startTime.add(Calendar.SECOND, REPORT_TEST_PERIOD_START);
		Calendar finalTime = Calendar.getInstance();
		finalTime.add(Calendar.SECOND, REPORT_TEST_PERIOD_END);
		
		t1.startTimer();
		t4.startTimer();
		TimeUnit.SECONDS.sleep(REPORT_TEST_WAIT_1);
		t1.stopTimer();
		t2.startTimer();
		TimeUnit.SECONDS.sleep(REPORT_TEST_WAIT_2);
		t4.stopTimer();
		t2.stopTimer();
		t3.startTimer();
		TimeUnit.SECONDS.sleep(REPORT_TEST_WAIT_3);
		t3.stopTimer();
		t2.startTimer();
		TimeUnit.SECONDS.sleep(REPORT_TEST_WAIT_4);
		t3.startTimer();
		TimeUnit.SECONDS.sleep(REPORT_TEST_WAIT_5);
		t2.stopTimer();
		t3.stopTimer();
		
		taskManager.printTable();
		
		//Brief report
		Report report = new BriefReport(startTime, finalTime, root);
		report.genReport();

		ReportGenerator repoGen = new HtmlReportGenerator();
		repoGen.generateReport(report);
		repoGen = new TextReportGenerator();
		repoGen.generateReport(report);
		
		//This is sleep is for not override the report files 
		TimeUnit.SECONDS.sleep(1); 
		
		//Detailed report
		report = new DetailedReport(startTime, finalTime, root);
		report.genReport();

		repoGen = new HtmlReportGenerator();
		repoGen.generateReport(report);
		repoGen = new TextReportGenerator();
		repoGen.generateReport(report);
		

	}
	
}