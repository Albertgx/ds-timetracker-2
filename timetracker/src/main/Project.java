package main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
* Represents a collection of tasks or projects as one unique activity
* that elapses on a different moments of time.
*/
public class Project extends Node {
	
	private static Logger logger = LoggerFactory.getLogger(Project.class);
	
	/*
	 * Id for the serialize implementation. 
	 */
	private static final long serialVersionUID = 8699152219902776968L;
	
	
	private boolean invariant() {
		boolean assertion = true;
		assertion = (assertion && this.subprojects != null);
		assertion = (assertion && this.getTime() >= 0);
		return assertion;
	}
	
	
	/** 
	 * Contains all the projects and tasks attached to this project.
	 * @uml.property name="subprojects"
	 * @uml.associationEnd readOnly="true" multiplicity="(0 -1)"
	 *  ordering="true" aggregation="shared" inverse="project:main.Node"
	 */
	private List<Node> subprojects;
	
	/*
	 * Project constructor.
	 * Parameters: name and description of the project.
	*/
	public Project(final String name, final String description) {
		super(name, description);
		this.subprojects = new ArrayList<Node>();
	}
	
	
	
	/*
	 *Iterate all sub projects and get an elapsed time. 
	*/
	public final double getTime() {
		
		
		double time = 0;
		Node aux;
		for (int i = 0; i < this.subprojects.size(); i++) {
			aux = this.subprojects.get(i);
			time = time + aux.getTime();
		}

		return time;
	}

	

	/** 
	 * Getter of the property <tt>subprojects</tt>
	 * @return  Returns the subprojects.
	 * @uml.property  name="subprojects"
	 */
	public final List<Node> getSubprojects() {
		return subprojects;
	}

		
	/*
	 *Add in sub projects list a new task.
	 *Parameters: name and description.
	*/
	public final void newTask(final String name, final String description) {
		
		assert this.invariant();
		Node newTask = new BaseTask(name, description);
		this.subprojects.add(newTask);
		logger.info("Task " + name + " has been created");
		assert this.invariant();
	}
	
	/*
	 *Parameters: name, description, timeLimit.
	 * Add a timeLimit task into sub projects list.
	*/
	public final void newTaskTimeLimit(final String name, 
							final String description, final double timeLimit) {
		assert this.invariant();
		Node newTask = new TimeLimit(
				new BaseTask(name, description), timeLimit);
		this.subprojects.add(newTask);
		logger.info("Task " + name + " has been created");
		assert this.invariant();
	}
	
	/*
	 * Parameters: name, description, scheduledTime.
	 * Creates a scheduledTime task into sub projects list.
	*/
	public final void newTaskScheduledTime(final String name, 
			final String description, final Calendar scheduledTime) {
		
		assert this.invariant();
		Node newTask = new ScheduledTime(
				new BaseTask(name, description), scheduledTime);
		this.subprojects.add(newTask);
		logger.info("Task " + name + " has been created");
		assert this.invariant();
	}
	
	/*
	 * Parameters: name, description, timeLimit.
	 * Add a scheduledtimeLimit task into sub projects list.
	*/
	public final void newTaskScheduledTimeLimit(final String name, 
			final String description, final double timeLimit,  
			final Calendar scheduledTime) {
		
		assert this.invariant();
		Node newTask = new TimeLimit(new ScheduledTime(
				new BaseTask(name, description), scheduledTime), timeLimit);
		this.subprojects.add(newTask);
		logger.info("Task " + name + " has been created");
		assert this.invariant();
	}
			
	/*
	* Parameters: name, description.
	*/
	public final void newProject(final String name, final String description) {
		
		assert this.invariant();
		Node newProject = new Project(name, description);
		this.subprojects.add(newProject);
		logger.info("Project " + name + " has been created");
		assert this.invariant();
	}
	
	

	public final Node getSubProject(final int index) {
		
		Node subNode = this.subprojects.get(index);
		return subNode;
	}


					
	/*
	 * Removes a task or a project form the subproject list
	 * Parameters: list index you want to remove
	*/
	public final void removeNode(final int index) {
		
		assert this.invariant();
		this.subprojects.remove(index);
		assert this.invariant();
	}

		
	/*
	 *Print project name, startTime, endTime and elapsedTime. 
	 *Time is printed in format hh:mm:ss.
	*/
	public final String print() {
		
		assert this.invariant();
		String projectString = "";
		String nom = this.getName();
		long sec = (long) this.getTime();
	
		String time = String.format("%02d:%02d:%02d", 
				TimeUnit.SECONDS.toHours(sec), 
				TimeUnit.SECONDS.toMinutes(sec) 
				- TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(sec)),
				sec - TimeUnit.SECONDS.toMinutes(sec));
		
		
		String startTimeStr = "";
		String endTimeStr = "";
		Calendar startTime = this.getStartTime();
		if (startTime != null) {
			startTimeStr = startTime.getTime().toString();
		}
		Calendar endTime = this.getEndTime();
		if (endTime != null) {
			endTimeStr = endTime.getTime().toString();
		}
		
		projectString = nom + "\t" + startTimeStr + "\t" 
						+ endTimeStr + "\t" + time + "\n";
		
		for (int i = 0; i < this.subprojects.size(); i++) {
			Node node = this.getSubProject(i);
			projectString += node.print();
		}
		assert this.invariant();
		return projectString;	
	}


	 /*
	 *Search the first moment a subtask or subproject started and get its time.
	 */
	public final Calendar getStartTime() {
		
		assert this.invariant();
		Calendar minStartTime = null;
		Node auxNode = null;
		Calendar auxStartTime = null;
		
		int size = this.subprojects.size();
		
		for (int i = 0; i < size; i++) {
			auxNode = this.subprojects.get(i);
			auxStartTime = auxNode.getStartTime();
			if (auxStartTime != null) {
				if (minStartTime == null) {
					minStartTime = auxStartTime; 
				} else if (auxStartTime.before(minStartTime)) {
					minStartTime = auxStartTime;
				}
			}
		}
		assert this.invariant();
		return minStartTime;
	}

	
	/*
	 *Search the last moment a subtask or subproject end or is in progress 
	*/
	public final Calendar getEndTime() {
		
		assert this.invariant();
		Calendar maxEndTime = null;
		Node auxNode = null;
		Calendar auxEndTime = null;
		
		int size = this.subprojects.size();
		
		for (int i = 0; i < size; i++) {
			auxNode = this.subprojects.get(i);
			auxEndTime = auxNode.getEndTime();
			if (auxEndTime != null) {
				if (maxEndTime == null) {
					maxEndTime = auxEndTime;
				} else if (auxEndTime.after(maxEndTime)) {
					maxEndTime = auxEndTime;
				}
			}
		}
		assert this.invariant();
		return maxEndTime;
	}


	/*
	 *Parameters: startTime, finalTime.
	 *Checks if any task, project or interval is between a specific time. 
	*/
	public final double getTimeInPeriod(final Calendar startTime, 
											final Calendar finalTime) {
		assert this.invariant();
		double time = 0;
		for (int i = 0; i < this.subprojects.size(); i++) {
			time += this.getSubProject(i).getTimeInPeriod(startTime, finalTime);
		}
		
		assert this.invariant();
		return time;
	}

	/*
	 *Implements visitor. 
	*/
	public final void accept(final TreeVisitor visitor) {
		visitor.visit(this);
	}
}