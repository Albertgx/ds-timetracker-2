package main;

/*
 * Abstract class for create functions
 * that visits the tree of projects 
 */
public abstract class TreeVisitor {
		
		public abstract void visit(Project project);
		public abstract void visit(Task task);
		public abstract void visit(Interval interval);


}
