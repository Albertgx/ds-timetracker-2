package main;

import java.util.List;

/*
 * Contains all the tasks and projects 
 */
public class TaskManager {

	/** 
	 * Contains all the subproject that client access
	 * @uml.property name="root"
	 * @uml.associationEnd readOnly="true" inverse="taskManager:main.Project"
	 */
	private Project root;
	
	private boolean invariant() {
		boolean assertion = true;
		assertion = this.root != null;
		return assertion;
	}
	
	
	/*
	 * TaskManager constructor
	 * Newroot is the project that contains all subproject that client access
	 */
	public TaskManager(final Project newRoot) {
		this.root = newRoot;
	}
	
	/** 
	 * Getter of the property <tt>root</tt>
	 * @return  Returns the root.
	 * @uml.property  name="root"
	 */
	public final Project getRoot() {
		return root;
	}
			
		
	/*
	 * Prints all the tasks and projects name,
	 * start time,end time and elapsed time created
	 */
	public final void printTable() {
		
		assert this.invariant();
		System.out.println("Name \t\t Start time \t\t\t " 
								+ "Final time \t\t Duration(hh:mm:ss)");
		System.out.println("------------------------------------" 
				+ "----------------------------------------------");
		List<Node> subprojects = this.root.getSubprojects();
		
		String treeString = "";
		
		for (int i = 0; i < subprojects.size(); i++) {
			treeString += subprojects.get(i).print();
		}
		if (treeString != null) { 
			System.out.println(treeString);
		}
		assert this.invariant();
	}
}