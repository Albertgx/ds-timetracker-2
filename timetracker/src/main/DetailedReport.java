package main;

import java.util.ArrayList;
import java.util.Calendar;

/*
 * Represents a report with
 * all the information about tasks and 
 * projects of the tree 
 */
public class DetailedReport extends Report {
	
	/*
	 * Detailed report constructor
	 */
	public DetailedReport(final Calendar newStartTime,
						  final Calendar newFinalTime,
			              final Project newRoot) {
		
		super(newStartTime, newFinalTime, newRoot);
	}
	
	/**
	 * @uml.property  name="PROJECTS_TABLE_COLUMNS" readOnly="true"
	 */
	private static final int PROJECT_TABLE_COLUMNS = 5;
	private static final int INTERVAL_TABLE_COLUMNS = 6;
	
	/*
	 * Generates a report, filling the report content
	 * with all the information 
	 */
	public final void genReport() {
		assert this.invariant();
		this.getContent().addHoritzontalLine();
		this.getContent().addTitle("Detailed report");
		this.getContent().addHoritzontalLine();
		this.getContent().addSubTitle("Period");
		
		//Period table
		Taula table = new Taula(0, 2);
		ArrayList<String> stringList = new ArrayList<String>();
		stringList.add("");
		stringList.add("Date");
		table.afegeixFila(stringList);
		
		stringList = new ArrayList<String>();
		stringList.add("Since");
		stringList.add(this.getStartTime().getTime().toString());
		table.afegeixFila(stringList);
		
		stringList = new ArrayList<String>();
		stringList.add("To");
		stringList.add(this.getFinalTime().getTime().toString());
		table.afegeixFila(stringList);
		
		stringList = new ArrayList<String>();
		stringList.add("Report generation date");
		stringList.add(this.getGenDate().getTime().toString());
		table.afegeixFila(stringList);
		
		this.getContent().addTable(table, true);
		this.getContent().addHoritzontalLine();
		this.getContent().addSubTitle("Root projects");

		
		table = new Taula(0, PROJECT_TABLE_COLUMNS);
		stringList = new ArrayList<String>();
		stringList.add("N.");
		stringList.add("Project");
		stringList.add("Start time");
		stringList.add("Final time");
		stringList.add("Elapsed time");
		table.afegeixFila(stringList);
		
		Project root = this.getRoot();
		
		int numProjects = root.getSubprojects().size();
		
		// If time > 0 means project is inside period Time and 
		//therefore we must add it.
		double time = 0;
		for (int i = 0; i < numProjects; i++) {
			
			Node subProject = root.getSubProject(i);
			time = subProject.getTimeInPeriod(this.getStartTime(),
					this.getFinalTime());
			if (time > 0) {
				stringList = new ArrayList<String>();
				stringList.add(Integer.toString(i + 1));
				stringList.add(subProject.getName());
				stringList.add(subProject.getStartTime().getTime().toString());
				stringList.add(subProject.getEndTime().getTime().toString());
				stringList.add(Double.toString(time));
				table.afegeixFila(stringList);
			}
		}
		
		this.getContent().addTable(table, false);
		
		
		
		this.getContent().addHoritzontalLine();
		this.getContent().addSubTitle("Subprojects");
		this.getContent().addDescription("In the next table is " 
				+ "only included subprojects that have any " 
				+ "task inside the period");
		
		//We use visitor in order to get Node's sons.
		ReportSubProjects visitorSubproject =
			new ReportSubProjects("", this.getStartTime(), this.getFinalTime());
		this.getRoot().accept(visitorSubproject);
		table = new Taula(0, PROJECT_TABLE_COLUMNS);
		stringList = new ArrayList<String>();
		stringList.add("N.");
		stringList.add("Project");
		stringList.add("Start time");
		stringList.add("Final time");
		stringList.add("Elapsed time");
		table.afegeixFila(stringList);
		table.afegirTaula(visitorSubproject.getSubProjects());
		this.getContent().addTable(table, false);
	
		
		
		this.getContent().addHoritzontalLine();
		this.getContent().addSubTitle("Tasks");
		this.getContent().addDescription("In the next table"
				+ " is included tasks's duration and "
				+ "the project they belong");
		
		//Table's table.
		table = new Taula(0, PROJECT_TABLE_COLUMNS);
		stringList = new ArrayList<String>();
		stringList.add("N. (sub)\n Project");
		stringList.add("Task");
		stringList.add("Start time");
		stringList.add("Final time");
		stringList.add("Elapsed time");
		table.afegeixFila(stringList);
		table.afegirTaula(visitorSubproject.getTasks());
		this.getContent().addTable(table, false);
		
		this.getContent().addHoritzontalLine();
		this.getContent().addSubTitle("Intervals");
		this.getContent().addDescription("In the next table"
				+ " is included the start,final and duration"
				+ " of all intervals inside the period."
				+ "There also are indicated the project and task"
				+ " where it belongs");
		
		// Interval's Table
		table = new Taula(0, INTERVAL_TABLE_COLUMNS);
		stringList = new ArrayList<String>();
		stringList.add("N. (sub)\n Project");
		stringList.add("Task");
		stringList.add("Interval");
		stringList.add("Start time");
		stringList.add("Final time");
		stringList.add("Elapsed time");
		table.afegeixFila(stringList);
		table.afegirTaula(visitorSubproject.getIntervals());
		this.getContent().addTable(table, false);
		
		this.getContent().addHoritzontalLine();	
		assert this.invariant();
	}
}